#ifndef CDICalibrator_new_H
#define CDICalibrator_new_H

#include "CalibrationDataInterface/CalibrationDataInterfaceBase.h"
#include "CalibrationDataInterface/CalibrationDataInterfaceROOT.h"



class CDICalibrator_new
{
  
public:
  
  CDICalibrator_new();

  ~CDICalibrator_new() {};
      
  double getSF(double pT, double eta, double eqEff,int flavor);

  double getUnc(double pT, double eta, double eqEff,int eigenvVariation,int flavor);

private:
  
  Analysis::CalibrationDataInterfaceROOT* cal;
  Analysis::Uncertainty unc;
  Analysis::CalibrationDataVariables btagjet; 
  std::string cut;

  double getTW(double eqEff);

  std::string getFlavor(int flavN);
  
};
    
  



#endif 
