#include "TH1F.h"
#include "TString.h"
#include "TFile.h"
#include <vector>
#include "RooRealVar.h"
#include "RooDataHist.h"

using namespace std;

void createRooDataHist()
{

  TString inputRootFile(TString("InputTransformedHistos.root"));
  TString outputRootFile(TString("RooDataHists.root"));
  
  vector<TString> namesOfInputHistos;
  namesOfInputHistos.push_back(TString("MC_b"));
  namesOfInputHistos.push_back(TString("MC_c"));
  namesOfInputHistos.push_back(TString("MC_l"));
  namesOfInputHistos.push_back(TString("data"));

  TFile* outputFile=new TFile(outputRootFile,"recreate");

  TFile* inputFile=new TFile(inputRootFile);
  
  int nBins=5;// was 7; -- A.X.

  RooRealVar* discr=new RooRealVar("discr","discr",nBins/2,0,nBins);

  for (unsigned int i=0;i<namesOfInputHistos.size();i++)
  {
    
    TH1F* histo=(TH1F*)inputFile->Get(namesOfInputHistos.at(i));

    RooDataHist* newDataset=new RooDataHist(namesOfInputHistos.at(i),
                                            namesOfInputHistos.at(i),
                                            RooArgList(*discr),
                                            histo,
                                            1);

    outputFile->cd();
    newDataset->Write();
  }

//  discr->Write();

  outputFile->Write();
  outputFile->Close();

  inputFile->Close();

}

  

