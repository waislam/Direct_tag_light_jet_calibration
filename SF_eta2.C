{
  
  gROOT->ProcessLine(".L ~/work/atlasstyle-00-03-05/AtlasStyle.C");

  SetAtlasStyle();

  gStyle->SetOptLogx();

//  gStyle->SetOptFit(111111)

  TFile* file=new TFile("combinedResults_sublead_mv2c10.root");
  
  TH1F* histoSF85_eta2=(TH1F*)file->Get("histoSF85_eta2");
  TH1F* histoSF77_eta2=(TH1F*)file->Get("histoSF77_eta2");
  TH1F* histoSF70_eta2=(TH1F*)file->Get("histoSF70_eta2");
  TH1F* histoSF60_eta2=(TH1F*)file->Get("histoSF60_eta2");

  TH1F* histoSF85sys_eta2=(TH1F*)file->Get("histoSF85sys_eta2");
  TH1F* histoSF77sys_eta2=(TH1F*)file->Get("histoSF77sys_eta2");
  TH1F* histoSF70sys_eta2=(TH1F*)file->Get("histoSF70sys_eta2");
  TH1F* histoSF60sys_eta2=(TH1F*)file->Get("histoSF60sys_eta2");

  histoSF85sys_eta2->SetLineColor(1);
  histoSF77sys_eta2->SetLineColor(1);
  histoSF70sys_eta2->SetLineColor(1);
  histoSF60sys_eta2->SetLineColor(1);

  histoSF85sys_eta2->SetFillColor(kGreen);
  histoSF77sys_eta2->SetFillColor(kGreen);
  histoSF70sys_eta2->SetFillColor(kGreen);
  histoSF60sys_eta2->SetFillColor(kGreen);


  TCanvas* cSF=new TCanvas("cSF_binbybin","cSF_binbybin",600,600);
  cSF->Divide(2,1);
  cSF->cd(1);

  histoSF85sys_eta2->GetYaxis()->SetRangeUser(0.,2.5);
//  histoSF85int_eta2->Fit("pol0");
  histoSF85sys_eta2->Draw("e2");
  histoSF85_eta2->Draw("pesame");

  cSF->cd(2);
  histoSF77sys_eta2->GetYaxis()->SetRangeUser(0.,2.5);
//  histoSF85int_eta2->Fit("pol0");
  histoSF77sys_eta2->Draw("e2");
  histoSF77_eta2->Draw("pesame");

 // cSF->cd(3);

 // histoSF70sys_eta2->GetYaxis()->SetRangeUser(0.,2.5);
////  histoSF85int_eta2->Fit("pol0");
 // histoSF70sys_eta2->Draw("e2");
 // histoSF70_eta2->Draw("pesame");

 // cSF->cd(4);

 // histoSF60sys_eta2->GetYaxis()->SetRangeUser(0.,2.5);
////  histoSF85int_eta2->Fit("pol0");
 // histoSF60sys_eta2->Draw("e2");
 // histoSF60_eta2->Draw("pesame");

  cSF->SaveAs("results/SFbinbybin_eta2.eps");
  

  TH1F* histoSF85int_eta2=(TH1F*)file->Get("histoSF85int_eta2");
  TH1F* histoSF77int_eta2=(TH1F*)file->Get("histoSF77int_eta2");
  TH1F* histoSF70int_eta2=(TH1F*)file->Get("histoSF70int_eta2");
  TH1F* histoSF60int_eta2=(TH1F*)file->Get("histoSF60int_eta2");

  TH1F* histoSF85sysint_eta2=(TH1F*)file->Get("histoSF85sysint_eta2");
  TH1F* histoSF77sysint_eta2=(TH1F*)file->Get("histoSF77sysint_eta2");
  TH1F* histoSF70sysint_eta2=(TH1F*)file->Get("histoSF70sysint_eta2");
  TH1F* histoSF60sysint_eta2=(TH1F*)file->Get("histoSF60sysint_eta2");

  histoSF85sysint_eta2->SetLineColor(1);
  histoSF77sysint_eta2->SetLineColor(1);
  histoSF70sysint_eta2->SetLineColor(1);
  histoSF60sysint_eta2->SetLineColor(1);

  histoSF85sysint_eta2->SetFillColor(kGreen);
  histoSF77sysint_eta2->SetFillColor(kGreen);
  histoSF70sysint_eta2->SetFillColor(kGreen);
  histoSF60sysint_eta2->SetFillColor(kGreen);


 cSF=new TCanvas("cSF_intAboveCut","cSF_intAboveCut",600,600);
  cSF->Divide(2,1);
  cSF->cd(1);
  histoSF85sysint_eta2->GetYaxis()->SetRangeUser(0.,2.5);
//  histoSF85int_eta2->Fit("pol0");
  histoSF85sysint_eta2->Draw("e2");
  histoSF85int_eta2->Draw("pesame");

  cSF->cd(2);
  histoSF77sysint_eta2->GetYaxis()->SetRangeUser(0.,2.5);
//  histoSF77int_eta2->Fit("pol0");
  histoSF77sysint_eta2->Draw("e2");
  histoSF77int_eta2->Draw("pesame");

  //cSF->cd(3);
  //histoSF70sysint_eta2->GetYaxis()->SetRangeUser(0.,2.5);
  //histoSF70sysint_eta2->Draw("e2");
  //histoSF70int_eta2->Draw("pesame");
////  histoSF70int_eta2->GetYaxis()->SetRangeUser(0.,2.);
////  histoSF70int_eta2->Draw();
  //cSF->cd(4);
  //histoSF60sysint_eta2->GetYaxis()->SetRangeUser(0.,2.5);
  //histoSF60sysint_eta2->Draw("e2");
  //histoSF60int_eta2->Draw("pesame");

////  histoSF60int_eta2->GetYaxis()->SetRangeUser(0.,2.);
////  histoSF60int_eta2->Draw();

  cSF->SaveAs("results/SFintegrated_eta2.eps");

  
  //TH1F* histoContent100_eta2=(TH1F*)file->Get("histoContent100_eta2");
  //TH1F* histoContent85_eta2=(TH1F*)file->Get("histoContent85_eta2");
  //TH1F* histoContent77_eta2=(TH1F*)file->Get("histoContent77_eta2");
  //TH1F* histoContent70_eta2=(TH1F*)file->Get("histoContent70_eta2");
  //TH1F* histoContent60_eta2=(TH1F*)file->Get("histoContent60_eta2");

  //TH1F* histoContentSys100_eta2=(TH1F*)file->Get("histoContentSys100_eta2");
  //TH1F* histoContentSys85_eta2=(TH1F*)file->Get("histoContentSys85_eta2");
  //TH1F* histoContentSys77_eta2=(TH1F*)file->Get("histoContentSys77_eta2");
  //TH1F* histoContentSys70_eta2=(TH1F*)file->Get("histoContentSys70_eta2");
  //TH1F* histoContentSys60_eta2=(TH1F*)file->Get("histoContentSys60_eta2");

  //histoContentSys100_eta2->SetLineColor(1);
  //histoContentSys85_eta2->SetLineColor(1);
  //histoContentSys77_eta2->SetLineColor(1);
  //histoContentSys70_eta2->SetLineColor(1);
  //histoContentSys60_eta2->SetLineColor(1);

  //histoContentSys100_eta2->SetFillColor(kGreen);
  //histoContentSys85_eta2->SetFillColor(kGreen);
  //histoContentSys77_eta2->SetFillColor(kGreen);
  //histoContentSys70_eta2->SetFillColor(kGreen);
  //histoContentSys60_eta2->SetFillColor(kGreen);


  //TH1F* histoContentMC100_eta2=(TH1F*)file->Get("histoContentMC100_eta2");
  //TH1F* histoContentMC85_eta2=(TH1F*)file->Get("histoContentMC85_eta2");
  //TH1F* histoContentMC77_eta2=(TH1F*)file->Get("histoContentMC77_eta2");
  //TH1F* histoContentMC70_eta2=(TH1F*)file->Get("histoContentMC70_eta2");
  //TH1F* histoContentMC60_eta2=(TH1F*)file->Get("histoContentMC60_eta2");


  //cSF=new TCanvas("cWeightFunction","cWeightFunction",600,600);
  //cSF->Divide(3,2);
  //cSF->cd(1);

  //histoContentSys100_eta2->GetYaxis()->SetRangeUser(0.85,1.);
  //histoContentSys100_eta2->Draw("e2");
  //histoContent100_eta2->Draw("same");
  //histoContentMC100_eta2->SetLineColor(2);
  //histoContentMC100_eta2->Draw("same");

  //cSF->cd(2);

  //histoContentSys85_eta2->GetYaxis()->SetRangeUser(0.,0.10);
  //histoContentSys85_eta2->Draw("e2");
  //histoContent85_eta2->Draw("same");
  //histoContentMC85_eta2->SetLineColor(2);
  //histoContentMC85_eta2->Draw("same");

  //cSF->cd(3);

  //histoContentSys77_eta2->GetYaxis()->SetRangeUser(0.,0.025);
  //histoContentSys77_eta2->Draw("e2");
  //histoContent77_eta2->Draw("same");
  //histoContentMC77_eta2->SetLineColor(2);
  //histoContentMC77_eta2->Draw("same");

  //cSF->cd(4);

  //histoContentSys70_eta2->GetYaxis()->SetRangeUser(0.,0.015);
  //histoContentSys70_eta2->Draw("e2");
  //histoContent70_eta2->Draw("same");
  //histoContentMC70_eta2->SetLineColor(2);
  //histoContentMC70_eta2->Draw("same");

  //cSF->cd(5);

  //histoContentSys60_eta2->GetYaxis()->SetRangeUser(0.,0.004);
  //histoContentSys60_eta2->Draw("e2");
  //histoContent60_eta2->Draw("same");
  //histoContentMC60_eta2->SetLineColor(2);
  //histoContentMC60_eta2->Draw("same");

  //cSF->SaveAs("results/TagWeightFunction_eta2.eps");

  //TH1F* histoContent85int_eta2=(TH1F*)file->Get("histoContent85int_eta2");
  //TH1F* histoContent77int_eta2=(TH1F*)file->Get("histoContent77int_eta2");
  //TH1F* histoContent70int_eta2=(TH1F*)file->Get("histoContent70int_eta2");
  //TH1F* histoContent60int_eta2=(TH1F*)file->Get("histoContent60int_eta2");

  //TH1F* histoContentSys85int_eta2=(TH1F*)file->Get("histoContentSys85int_eta2");
  //TH1F* histoContentSys77int_eta2=(TH1F*)file->Get("histoContentSys77int_eta2");
  //TH1F* histoContentSys70int_eta2=(TH1F*)file->Get("histoContentSys70int_eta2");
  //TH1F* histoContentSys60int_eta2=(TH1F*)file->Get("histoContentSys60int_eta2");

  //TH1F* histoContentMC85int_eta2=(TH1F*)file->Get("histoContentMC85int_eta2");
  //TH1F* histoContentMC77int_eta2=(TH1F*)file->Get("histoContentMC77int_eta2");
  //TH1F* histoContentMC70int_eta2=(TH1F*)file->Get("histoContentMC70int_eta2");
  //TH1F* histoContentMC60int_eta2=(TH1F*)file->Get("histoContentMC60int_eta2");

  //histoContentSys85int_eta2->SetLineColor(1);
  //histoContentSys77int_eta2->SetLineColor(1);
  //histoContentSys70int_eta2->SetLineColor(1);
  //histoContentSys60int_eta2->SetLineColor(1);

  //histoContentSys85int_eta2->SetFillColor(kGreen);
  //histoContentSys77int_eta2->SetFillColor(kGreen);
  //histoContentSys70int_eta2->SetFillColor(kGreen);
  //histoContentSys60int_eta2->SetFillColor(kGreen);



  //cSF=new TCanvas("cWeightFunction_aboveCut","cWeightFunction_aboveCut",600,600);
  //cSF->Divide(2,2);

  //cSF->cd(1);

  //histoContentSys85int_eta2->GetYaxis()->SetRangeUser(0.,0.1);
  //histoContentSys85int_eta2->Draw("e2");
  //histoContent85int_eta2->Draw("same");
  //histoContentMC85int_eta2->SetLineColor(2);
  //histoContentMC85int_eta2->Draw("same");

  //cSF->cd(2);
  //histoContentSys77int_eta2->GetYaxis()->SetRangeUser(0.,0.03);
  //histoContentSys77int_eta2->Draw("e2");
  //histoContent77int_eta2->Draw("same");
  //histoContentMC77int_eta2->SetLineColor(2);
  //histoContentMC77int_eta2->Draw("same");

  //cSF->cd(3);
  //histoContentSys70int_eta2->GetYaxis()->SetRangeUser(0.,0.01);
  //histoContentSys70int_eta2->Draw("e2");
  //histoContent70int_eta2->Draw("same");
  //histoContentMC70int_eta2->SetLineColor(2);
  //histoContentMC70int_eta2->Draw("same");

  //cSF->cd(4);
  //histoContentSys60int_eta2->GetYaxis()->SetRangeUser(0.,0.004);
  //histoContentSys60int_eta2->Draw("e2");
  //histoContent60int_eta2->Draw("same");
  //histoContentMC60int_eta2->SetLineColor(2);
  //histoContentMC60int_eta2->Draw("same");

  //cSF->SaveAs("results/TagWeightFunctionIntegrated_eta2.eps");

  //TH1F* histoFlavCompB_eta2=(TH1F*)file->Get("histoFlavCompB_eta2");
  //TH1F* histoFlavCompC_eta2=(TH1F*)file->Get("histoFlavCompC_eta2");

  //TH1F* histoFlavCompSysB_eta2=(TH1F*)file->Get("histoFlavCompSysB_eta2");
  //TH1F* histoFlavCompSysC_eta2=(TH1F*)file->Get("histoFlavCompSysC_eta2");

  //histoFlavCompSysB_eta2->SetFillColor(kGreen);
  //histoFlavCompSysC_eta2->SetFillColor(kGreen);

  //TH1F* histoFlavCompB_MC_eta2=(TH1F*)file->Get("histoFlavCompB_MC_eta2");
  //TH1F* histoFlavCompC_MC_eta2=(TH1F*)file->Get("histoFlavCompC_MC_eta2");

  //cSF=new TCanvas("FlavComb","FlavComp",600,600);
  //cSF->Divide(2);

  //cSF->cd(1);

  //histoFlavCompSysB_eta2->GetYaxis()->SetRangeUser(0.,0.06);
////  histoSF85int_eta2->Fit("pol0");
  //histoFlavCompSysB_eta2->Draw("e2");
  //histoFlavCompB_eta2->Draw("pesame");
  //histoFlavCompB_MC_eta2->SetLineColor(2);
  //histoFlavCompB_MC_eta2->Draw("same");

  //cSF->cd(2);
  //histoFlavCompSysC_eta2->GetYaxis()->SetRangeUser(0.,0.2);
////  histoSF85int_eta2->Fit("pol0");
  //histoFlavCompSysC_eta2->Draw("e2");
  //histoFlavCompC_eta2->Draw("pesame");
  //histoFlavCompC_MC_eta2->SetLineColor(2);
  //histoFlavCompC_MC_eta2->Draw("same");

  //cSF->SaveAs("results/FlavFractions_eta2.eps");

  //TH1F* histoFitCovQualStatus_eta2=(TH1F*)file->Get("histoFitCovQualStatus_eta2");
  //TH1F* histoFitStatus_eta2=(TH1F*)file->Get("histoFitStatus_eta2");


  //cSF=new TCanvas("FlavFractions","FlavFractions",600,600);

  //cSF->Divide(2);

  //cSF->cd(2);
  //histoFitCovQualStatus_eta2->Draw();

  //cSF->cd(1);
  //histoFitStatus_eta2->Draw();

  //cSF->SaveAs("results/FitStatus_eta2.eps");
  

}
