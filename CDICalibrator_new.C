#include "CDICalibrator_new.h"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <utility>

using std::cout;
using std::cerr;
using std::endl;


CDICalibrator_new::CDICalibrator_new()
{
  //std::string root="SF.root";
  std::string root="2017-21-13TeV-MC16-CDI-2018-02-09_v1.root" ; //"2016-20_7-13TeV-MC15-CDI-2017-06-07_v2.root"; //"2017-21-13TeV-MC16-CDI-2017-12-22_v1.root";
  std::string tagger= "MV2c10";  //"DL1";
  std::string analysis="default";
  std::string analysisB="default";

  cut= "Continuous"; //"FixedCutBEff_70"; //"FixedCutBEff_85"; //"HybBEff_85"; // "Continuous";
  
  btagjet.jetAuthor  = "AntiKt4EMTopoJets";

  unc =  Analysis::Total;

  std::string file = root; 
  file+=".env";
  std::ofstream fout(file.c_str());
  fout << "File: " << root <<endl;
  fout << tagger << ".ScaleFactorCalibrationB" << "Name: " << analysisB << endl;
  fout << tagger << ".ScaleFactorCalibrationC" << "Name: " << analysis << endl;
  fout << tagger << ".ScaleFactorCalibrationLight" << "Name: " << analysis << endl;

  fout << "runEigenVectorMethod:     true" << endl;
  fout.close();

  cal=new Analysis::CalibrationDataInterfaceROOT(tagger,file);

}

double CDICalibrator_new::getTW(double eqEff)
{

 int TW=0;
 if (eqEff>=0 && eqEff<0.30)
 {
   TW=6;
 }
 else if (eqEff>=0.30 && eqEff<0.50)
 {
   TW=5;
 }
 else if (eqEff>=0.50 && eqEff<0.60)
 {
   TW=4;
 }
 else if (eqEff>=0.60 && eqEff<0.70)
 {
   TW=3;
 }
 else if (eqEff>=0.70 && eqEff<0.77)
 {
   TW=2;
 }
 else if (eqEff>=0.77 && eqEff<0.85)
 {
   TW=1;
 }
 else if (eqEff>=0.85 && eqEff<=1.)
 {
   TW=0;
 }
 else 
 {
   std::cout << " Efficiency in invalid range (0-1): " << eqEff;
 }
 
 ///double tagW[6] = { 0,0.4050,0.7028,0.8353,0.92371,1.};
 double tagW[8] = { -1.,-0.7887,-0.4434,-0.0436,0.4496,0.7535,0.940,1.};
 
 cout << " efEff " << eqEff << " --> weight " << ((tagW[TW] + tagW[TW+1])/2.) << endl;

  return  ((tagW[TW] + tagW[TW+1])/2.);

}

std::string CDICalibrator_new::getFlavor(int flavN)
{
  if (flavN==5)
  {
    return "B";
  }
  else if (flavN==4)
  {
    return "C";
  }
  else if (flavN==15)
  {
    return "T";
  }
  else return "Light";
}

double CDICalibrator_new::getSF(double pT, double eta, double eqEff,int flavor)
{
  
  btagjet.jetPt = pT;
  btagjet.jetEta = eta;
  btagjet.jetTagWeight = getTW(eqEff);
  std::string flavString=getFlavor(flavor);
 
  double SF=cal->getScaleFactor(btagjet, flavString, cut, Analysis::Total).first;;
  
 // if (flavString =="C")
 // {
 //   SF = SF / 1.2;
 // }
 // //else if (flavString =="Light")
 // //{
 // //  SF = SF /1.5;
 // //}
 // else if (flavor==0 && SF>1) 
 // { 
 //   SF/=1.5;
 // }

  std::cout << " tagWeight: " << btagjet.jetTagWeight << " pT: " << pT << " eta: " << eta << " flav " << flavString << " Sf " << SF << std::endl;

  return SF;
}

double CDICalibrator_new::getUnc(double pT, double eta, double eqEff,int eigenVariation,int flavor)
{

//WARNING WARNING WARNING

  double extrapolationFactor=1;

  btagjet.jetPt = pT;
  btagjet.jetEta = eta;
  btagjet.jetTagWeight = getTW(eqEff);
  std::string flavString=getFlavor(flavor);
  
  std::pair<double,double> pairVal = cal->getScaleFactor(btagjet, flavString, cut, Analysis::SFEigen,eigenVariation);

  std::cout << " tagWeight: " << btagjet.jetTagWeight << " pT: " << pT << " eta: " << eta << " flav " << flavString << " unc " << (pairVal.first-pairVal.second)/2. << std::endl;


  return extrapolationFactor*(pairVal.first-pairVal.second)/2.;//this is a signed uncertainty

}




// //MV1c

//std::pair<double,double> SFtot = cal->getScaleFactor(btagjet, flavString, cut, Analysis::Total);

//std::pair<double,double> SFtot = cal->getScaleFactor(btagjet, flavString, cut, Analysis::SFEigen,0);

//int numVariations=cal->getNumVariations(btagjet.jetAuthor,flavString,cut,Analysis::SFEigen);


