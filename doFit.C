{

  gSystem->Load("/afs/cern.ch/user/w/waislam/work/testarea/packages/RootCore/lib/generic/libCalibrationDataInterface.so");

  gROOT->ProcessLine(".L ~/work/atlasstyle-00-03-05/AtlasStyle.C");

  SetAtlasStyle();

  gROOT->ProcessLine(".L CDICalibrator_new.C+");
  gROOT->ProcessLine(".L transformInputs.C+");
  gROOT->ProcessLine(".L createRooDataHist.C+");
  gROOT->ProcessLine(".L RhhBinnedPdf.cc+");
  gROOT->ProcessLine(".L buildUpModel.C+");
  gROOT->ProcessLine(".L runFit.C+");
  bool mergeEtaBins=false;
  int nEtaBins=2;
  
  if (mergeEtaBins) nEtaBins=1;
  
//

//  int startSystematics=44;
//  int endSystematics=44;

//
  
  int startSystematics=-1;
//  int startSystematics=0;
//  int startSystematics=-1;
//  int endSystematics=1;
  int endSystematics= 10;
//  int endSystematics=-1;

//

//  int nSys=-1;
  for (int nSys=startSystematics;nSys<endSystematics+1;nSys++)
  {
    for (int pTbin=1;pTbin<9;pTbin++)
//  int pTbin=3;
    {
      for (int etabin=1;etabin<nEtaBins+1;etabin++)
//      int etabin=1;
      {
        
        std::cout << " Processing pTbin: " << pTbin << " and etabin " << etabin << std::endl;
        if (mergeEtaBins)
        {
          transformInputs(pTbin,0,nSys);
        }
        else
        {
          transformInputs(pTbin,etabin,nSys);
        }
        
        createRooDataHist();
        runFit(pTbin,etabin,nSys);
        
      }
    }
  }

  //gROOT->ProcessLine(".x generatePlots.C+");

  
}



