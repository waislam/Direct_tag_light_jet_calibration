Instructions:

1) First source root

source setupROOT.sh

2) Then install the CalibrationDataInterface using RootCore

the packages.txt file content should look like:
atlasoff/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/CalibrationDataInterface/trunk

the package here assumes the library is here:
RootCore/lib/x86_64-slc6-gcc48-opt/libCalibrationDataInterface.so"

if this will be in a different location, just change this in doFit.C

2b) you need to also link the header files of the CalibrationDataInterface, for this just link the header file directory to a directory with the same name in the directory of the fit macros, e.g.

ln -s MyPath/RootCore/CalibrationDataInterface/CalibrationDataInterface CalibrationDataInterface

3) Get the latest CDI file (2017-21-13TeV-MC16-CDI-2018-02-09_v1.root) from : http://atlas.web.cern.ch/Atlas/GROUPS/DATABASE/GroupData/xAODBTaggingEfficiency/13TeV/

4) One RootCore is properly installed, you can run all the fits

>root -b
>{ gROOT->ProcessLine(".x doFit.C");} >& log.txt 
press CTR-Z
>bg
(to put it in background)
>tail -f log.txt

This will put all the log file into the log.txt file

The plots of the single fits go into the plots/ directory.

5) Then you generate the plots with:

>root -b
>.x generatePlots.C+(true)

(the bool refers to whether add or not systematics)

6) make the plots

> root -l 
> .x makePlots.C (central eta)
> .x makePlotsEta2.C (forward eta)

The overall result plots go in the directory results/